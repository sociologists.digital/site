# TechnoSoc: Sociologists of Digital Things

*Compiled by Matt Rafalow, last updated: 6/15/21*

---

## Welcome!

This is a living document (meaning, it will be updated over time) to help orient new folks to the Slack. The goal here is to show you around.


## General guidance 

If you’ve never used Slack before, I recommend checking out this brief [overview](https://togetherfor2020.org/resources/slack-for-beginners/) of how it works. There you’ll learn the basics: what a channel is, how to interact, how to set up your profile, etc.


### What this group is and how to join

This Slack channel is for sociologists who study digital phenomena in any capacity and/or who use new digital methods to study social life. We help one another with questions we have about research, readings, teaching, jobs, and more. We also organize community activities for those who are interested. The only condition for entry is a substantive interest in the sociology of digital life (a student interested in the field, a professor studying this work, an industry researcher, etc.). To join, apply [here](https://forms.gle/pTbGkCVE1THNkeda9). We encourage [downloading](https://slack.com/downloads) the app because it makes it easier to participate.


### Expectations for participation

Once you’re in, you should know what is expected to participate – check out the [rules](https://sociologistsdigital.slack.com/archives/C0183MY6HRQ) before sending messages across the community.


### Channels

Channels are discussion rooms that are tailored to different topics and purposes. You can find them on the left hand side (marked with #) of your Slack interface. Here is our current list and a general overview of what happens in each:

[#ai_ml](https://app.slack.com/client/T017WKK46K0/C01HDGT668J/files/F01FF4TS2RW):				Discuss artificial intelligence, machine learning, and society.

[#all_things_bipoc_poc](https://app.slack.com/client/T017WKK46K0/C01JJHMFNDB/files/F01FF4TS2RW):	Discuss race/ethnicity in research.

[#amas](https://app.slack.com/client/T017WKK46K0/C01JELS4JET/files/F01FF4TS2RW):			Events where we “ask me anything” (AMA) with invited quests.

[#celebration](https://sociologistsdigital.slack.com/archives/C0189TP6VNY):			A new publication, a new job...a new puppy? Celebrate it here.

[#conferences_cfps](https://sociologistsdigital.slack.com/archives/C0185RZ5NDB):		Share or read about upcoming conferences and calls for papers.

[#community_website](https://technosoc.slack.com/archives/C01PJHYM5HB):		Weigh in on what goes on the TechnoSoc website.

[#dataresources](https://sociologistsdigital.slack.com/archives/C018KNXRMTK):		Information about datasets to use or related topics.

[#ethnography](https://technosoc.slack.com/archives/C0242MKLJV6):		Talk about fieldwork methods, digital ethnography and related topics.

[#games_and_research](https://app.slack.com/client/T017WKK46K0/C01JD5UMLPQ/files/F01FF4TS2RW):	Discuss social, cultural, human components of video games.

[#general](https://sociologistsdigital.slack.com/archives/C018D7E7PN0):			General watercooler talk – no topical constraints!

[#industry_mentorship](https://app.slack.com/client/T017WKK46K0/C01J38H7JKG/files/F01FF4TS2RW):	Participate in a mentoring program for industry work.

[#industrywork](https://technosoc.slack.com/archives/C01PMSJM34K)			Discuss topics related to those currently working in industry.

[#internships](https://sociologistsdigital.slack.com/archives/C018DH11C7N): 			Information about internships: how to apply, where to apply.

[#influencing](https://app.slack.com/client/T017WKK46K0/C01HVN1LCAH/files/F01FF4TS2RW):			Discuss influencing/influencer culture. Also juicy YouTube drama.

[#intros](https://sociologistsdigital.slack.com/archives/C0183BMHDDG):			New to the Slack? Introduce yourself here!

[#internships](https://app.slack.com/client/T017WKK46K0/C018DH11C7N/files/F01FF4TS2RW):			Discuss all things related to internships – applying, etc.

[#labor](https://app.slack.com/client/T017WKK46K0/C01HML1NXGC/files/F01FF4TS2RW):				Discuss work, labor, and technology research.

[#misinfo_polarization](https://app.slack.com/client/T017WKK46K0/C01HFEQ4W5D/files/F01FF4TS2RW):		Discuss research and news about online misinfo and polarization.

[#mondaystandup](https://sociologistsdigital.slack.com/archives/C0184TCFWFR):		Where we host our Monday ritual to share what you’re up to.

[#news](https://app.slack.com/client/T017WKK46K0/C01FQ3WEN5V/files/F01FF4TS2RW):				Discuss recent news articles relevant to the community.

[#nlp_text_and_language](https://sociologistsdigital.slack.com/archives/C018610C18X):	Discuss computational methods for analyzing textual corpora.

[#novelsnovelsnovels](https://app.slack.com/client/T017WKK46K0/C01FRELKY12/files/F01FF4TS2RW):		Share and discuss non-academic reading.

[#postphd_academicjobs](https://sociologistsdigital.slack.com/archives/C01826N6V53):	Post academic jobs, read about such jobs, discuss such jobs.

[#postphd_industry](https://sociologistsdigital.slack.com/archives/C0187BKG8JE):		Post industry jobs, read about such jobs, discuss such jobs.

[#publicwriting](https://app.slack.com/client/T017WKK46K0/C01HY3ARKGV/files/F01FF4TS2RW):			Learn about how to write about research for public audiences.

[#researchquestions](https://sociologistsdigital.slack.com/archives/C018G6GAHB3):		Ask for advice on a project, reading suggestions, etc.

[#rules](https://sociologistsdigital.slack.com/archives/C0183MY6HRQ):				Learn about the rules for participation and suggest new ones.

[#students](https://sociologistsdigital.slack.com/archives/C018F4U2RFX):			Meet other students, discuss topics relevant to other students.

[#teachingquestions](https://sociologistsdigital.slack.com/archives/C01809Y9R8W):		Ask for advice about teaching, suggested readings, etc.

[#uprooted](https://technosoc.slack.com/archives/C025MMNP62C):		Connecting members who live, study or work far from their "roots."

### Community activities

We also have a number of ongoing community activities that you are free to participate in or even help lead. Feel free to reach out directly to the committee members to join:

**Ask Me Anything (AMA)**

Committee: Angèle Christin, Daisy Lu, Matt Rafalow



*   Styled from Reddit AMAs, our TechnoSoc [#amas](https://app.slack.com/client/T017WKK46K0/C01JELS4JET/files/F01FF4TS2RW) invite guests to participate in a Q&A guided by questions from the Slack community. After the AMA is over, we share the discussion externally (see our archive [here](https://docs.google.com/document/d/1bzR1Okyswnb0CuxQV8ZuKCuKOTHeHGt8LBYHU0ilzVE/edit#heading=h.v6n2frrtxkym)).

**Computational Social Science Reading Group**

Committee: Nga Than



*   The reading group meets bi-weekly on Fridays 11-12pm (Eastern Time). At each meeting, we do a close reading of one CSS article, sometimes members try to reproduce the code in the article. We maintain [a reading list](https://docs.google.com/document/d/1bexPOiSgxwqSnw4iZdv708sewb_fXypPgaabDMSDnfk/edit?usp=sharing), which has our schedule, a Zoom link, and often choose the bi-weekly reading from this list. 

**Beverage-agnostic TechnoSoc Happy Hours**

Committee: Christopher Persaud, Liz Marquis



*   A recurring, region-friendly, low-stakes ways for members to hang out and participate in ice breaker-esque activities.

**Expert Database**

Committee: Matt Rafalow



*   People deserve to know about you and your work. Members can [submit](https://docs.google.com/forms/d/e/1FAIpQLScDhou4m_sEgchAUA26gUDhmkgdcTA7J5ATQBH0Y2_a4w_XeA/viewform?usp=sf_link) themselves to the expert database for others (in and outside of the Slack) to invite them for class lectures, consulting, journalist contact, etc. Anyone can view the database [here](https://docs.google.com/spreadsheets/d/1j3Y8aEGcEvvpeepSK5mKlZBUF00pVbgE4CeTLNEY5b0/edit#gid=122391586).

**Industry Mentoring Program**

Committee: Michael Dickard, Sam Jaroszewski, Michael Miner



*   A mentoring program to connect mentors and mentees for learning about industry careers. Join [#industry_mentorship](https://app.slack.com/client/T017WKK46K0/C01J38H7JKG/files/F01FF4TS2RW) to participate.

**Mini-Workshop Series**

Committee: Daisy Lu, Lucy Li



*   Workshop (coming soon!) for members to share about topics they care about, including to teach particular research skills, like methods or aspects of literatures, job application/resume workshops, etc.

**Monday Standups**

Committee: Matt Rafalow



*   Standups, borrowed from tech land, are a weekly ritual to share something about yourself: what you’re working on, what you’re thinking about, or even questions you have for others. It keeps the community alive, engaged, and creates opportunities for connection. This happens every Monday in the channel.

**Randomized Coffee Trials (RCT)**

Committee: Michael Soto



*   Text is great for communication, but sometimes one to one meetups offer a bit more. This activity stream is an opt-in program to randomly match interested members with one another for scheduled coffees over video conference. Each month you are introduced to someone new. Sign up [here](https://app.sparkcollaboration.com/index.php/participantreg?activation_key=5e6bc8ad64e6abb8b9105773532dcb7e&selfregister=yes&lang=en)!

**Social Scientists on the Market**

Committee: John Boy, Liza Shifrin, Nga Than



*   A [public-facing listing](https://otm.sociologists.digital/) of members who are on the job market, either for academic gigs or careers in industry, the public sector, or civil society organizations. 

**Sociologists of Digital Things Syllabus Share**

Committee: Diana Enriquez, Liza Shifrin



*   Sociologists are increasingly teaching courses on digital phenomena, but we don’t have a lot of models right now within the discipline. This resource (coming soon) will share syllabi of interest to members to help with their teaching and research.

**Welcome Wagon**

Committee: Lucy Li, Nga Than, Matt Rafalow



*   We have a lot of new members, and more every day. Some online communities have community members who make an effort to welcome new people, help people with any questions about the community, and keep discussions going.

**Who’s Who in the Slack**



*   With as many members as we have, it’s hard to know who is here and what types of communication and collaboration others are open to. View the ever growing community ‘yearbook’ [here](https://docs.google.com/presentation/d/1Pfofe-qW22Ks3tVXimpdd7mjoVN6eREcO9mOOXcgovk/edit#slide=id.p), and add your own entry.

**Writing accountability groups**

Committee: Morgan Johnstonbaugh



*   The writing accountability group meets every week on Wednesdays from 10-11 Eastern time (New York). During our weekly meetings we discuss our short-term writing goals and dedicate the majority of our time to writing “together”. Once a month, we read work written by two group members and provide constructive feedback. If you’re interested in joining us, please email mjohnst2@email.arizona.edu for our Zoom meeting details.


## Specific interests


### I am looking for a job



*   Be sure to regularly check out [#postphd_academicjobs](https://sociologistsdigital.slack.com/archives/C01826N6V53) and [#postphd_industryjobs](https://sociologistsdigital.slack.com/archives/C0187BKG8JE). You can use both of these channels to find job postings and, critically, to discuss questions you might have about the application process.
*   Those of us who work as social scientists in industry are trying to demystify the process. We created a living document [here](https://docs.google.com/document/d/1elmKpsOYCbPy0kF5eqXewvM3rQck8Y4nQ7aTBlIeUqo/edit?ts=5f613872#) to provide guidance around industry research work. We also have an [#industry_mentorship](https://app.slack.com/client/T017WKK46K0/C01J38H7JKG/files/F01FF4TS2RW) program you can join, either as a mentee or a mentor. [#internships](https://app.slack.com/client/T017WKK46K0/C018DH11C7N/files/F01FF4TS2RW) is another important part of the tech industry hiring process.


### I need help with a research project



*   Research is the bread and butter of what we do, and this Slack is designed to be a space where you can get help and offer feedback to others on any phase of their research about digital things.
*   [#researchquestions](https://sociologistsdigital.slack.com/archives/C018G6GAHB3) is the key channel to ask for advice or offer it: Which methods should I use? What readings should I check out for a particular topic? Is anyone free to review a draft of a paper? What advice do folks have for where to submit my paper? Who wants to collaborate? Ask it all here, and help others with those questions.
*   We also have a growing number of channels that focus on specific aspects of the research process. They include [#dataresources](https://sociologistsdigital.slack.com/archives/C018KNXRMTK), for those who need access to datasets or want to share them or discuss data in general (who doesn’t love talking about data?); as well as [#nlp_text_and_language](https://sociologistsdigital.slack.com/archives/C018610C18X) for researchers interested in using computational methods for analyzing text. We have an ever-growing list of channels for research sub-topics: [#ai_ml](https://app.slack.com/client/T017WKK46K0/C01HDGT668J/files/F01FF4TS2RW), [#all_things_bipoc_poc](https://app.slack.com/client/T017WKK46K0/C01JJHMFNDB/files/F01FF4TS2RW), [#games_and_research](https://app.slack.com/client/T017WKK46K0/C01JD5UMLPQ/files/F01FF4TS2RW), [#influencing](https://app.slack.com/client/T017WKK46K0/C01HVN1LCAH/files/F01FF4TS2RW), [#labor](https://app.slack.com/client/T017WKK46K0/C01HML1NXGC/files/F01FF4TS2RW), and [#misinfo_polarization](https://app.slack.com/client/T017WKK46K0/C01HFEQ4W5D/files/F01FF4TS2RW).


### I need help with my teaching



*   [#teachingquestions](https://sociologistsdigital.slack.com/archives/C01809Y9R8W) is a great place to ask for advice about teaching or give advice to others: What suggested texts would others recommend for a particular course topic? What digital approaches to instruction might help my teaching? Ask it all here.


### I want to celebrate a milestone



*   We all know that as researchers our milestones are few during what can be the quite lengthy process that is scholarly work. You are encouraged to share your milestones in [#celebration](https://sociologistsdigital.slack.com/archives/C0189TP6VNY), and you are also encouraged to celebrate others’ milestones. Some ideas for what to celebrate:
    *   Admission to a graduate program
    *   A new job
    *   Passing an exam
    *   Publishing a paper
    *   Being interviewed for an article or writing an article that is shared to the public
    *   A new puppy
    *   I mean, seriously – the world needs more joy – celebrate! 


### I live outside North America



*   You're not alone -- we're an international community! Join [#africa](https://technosoc.slack.com/archives/C020MB9LTN1), [#europe](https://technosoc.slack.com/archives/C01HLDUV8JK), [#south-and-se-asia](https://technosoc.slack.com/archives/C01HWFH7HAS) to connect with others in your region, and discuss the global life in [#uprooted](https://technosoc.slack.com/archives/C025MMNP62C).
