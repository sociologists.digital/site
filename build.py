import os
from datetime import datetime

from mistune import markdown
from emoji import emojize
from smartypants import smartypants
from staticjinja import Site


def render_markdown(md):
    return smartypants(markdown(emojize(md, use_aliases=True), escape=False))


def md_pages(site, template, **kwargs):
    for page in os.listdir("pages"):
        page_name, _ = page.split(".")
        outdir = os.path.join(site.outpath, page_name)
        dest = os.path.join(outdir, "index.html")
        with open(os.path.join("pages", page)) as f:
            file_contents = f.read()
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        template.stream({"content": file_contents}).dump(dest, encoding="utf-8")


def now():
    return datetime.utcnow().isoformat()


config = {
    "footer_links": [
        ("Slack", "https://sociologistsdigital.slack.com/"),
        ("Application Form", "https://forms.gle/DkzJaWjTHk1mXjEF6"),
        ("Starter Guide", "/starter-guide"), # "https://docs.google.com/document/d/1nV4XNu-YGlI1W7E2htSRwut6obdXU4jnBk83ZSKV_xE/"
        ("On the Market", "https://otm.sociologists.digital/"),
        ("AMAs", "/amas"),
    ]
}


if __name__ == "__main__":
    site = Site.make_site(
        outpath="public",
        rules=[("page.html", md_pages)],
        filters={"markdown": render_markdown},
        env_globals={"now": now(), "site": config},
    )
    site.render()
