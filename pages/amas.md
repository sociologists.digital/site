# TechnoSoc: Ask Me Anything (AMA)
#### *Compiled by Matt Rafalow, Daisy Lu, Lucy Li, and Angèle Christin (last update: 9/30/21)*

## What is AMA?

Ask Me Anything (AMA) is an activity styled from other platforms (like Reddit) where guests are invited to participate in a Q&A guided by questions from the community. AMAs are an opportunity to get to know our members better and to share their answers in archive format more broadly.

## AMAs

### Kelsey Gonzalez and Helge Marahrens (September 23, 2021)

*Kelsey Gonzalez is a PhD candidate at the University of Arizona who recently completed her Data Science internship at IBM’s Chief Analytics Office.*

*Helge Marahrens is a PhD student at Indiana University who just completed his internship at Facebook’s Core Data Science group.*

*Kelsey:* Happy to be here and see how Helge and I can answer your questions! Let's dig in .

*Helge:* Same here! Thanks for the great questions and looking forward to the discussion.

__What was the most surprising thing you learned during your internship?__

*Helge:* How much we coded in SQL. It was useful because it allowed us to access more data, but it also connects to a larger point in that a lot of the analyses are broad rather than deep. Descriptives are key and we spent a lot of time finding a direction before committing and going deeper (e.g. regression models, ML etc.) For descriptives, SQL was more than enough.

*Kelsey:* For me, it was how important team/company culture is. Perhaps in some companies teams can be much more competitive and 'cutthroat', but my team was incredibly supportive and fostered my own learning and growth. Unexpectedly they had complete faith in me and my abilities which, in turn, led me to overcome a lot of imposter syndrome coming into this new space. I'm not sure how to create this space in a team, but I found it so surprising that my project managers were able to foster such a positive environment.

__Any thoughts on the comparison of “work culture” from industry to academia? Is your sense that one is more or less cutthroat/collaborative/etc. than the other?__

*Kelsey:* At least for me, both places haven't been especially cutthroat. However, the stronger collaborative environment in industry really felt comfortable for me as I'm big on co-authoring and working in teams. If you're a lone wolf, that's a bit harder! I know that non-academics often have a stereotype that phd students/academics tend to be pretty independent and work poorly in teams, though idk where that comes from. That said, it's important to signal you're collaborative and work well in teams in your application materials and interviews; they are hiring someone who can do the job but also someone who they will enjoy working with.

__What sort of skills did you use the most during the internship?__

*Helge:* SQL and Python, but halfway through my internship my advisor reminded me that I can run the fanciest analyses – they only matter to the extent that I can communicate them well. What did I find and why does it matter? Presentation/communication skills using PowerPoint and shared documents became more important during the second half.

*Kelsey:* My role involved a lot of coding as an individual contributor, so there's that, but overall here are the skills I felt that I grew during the summer: communication skills, team management, python & pandas (though I'm an R user at heart), object-oriented programming, complex thinking, managing different stakeholder interests, learning when and how best to speak up and challenge others on their ideas.

__How did you find and apply for the internship?__

*Helge:* I worked with my advisor at ICPSR last year. When a spot opened up on his team, he asked me if I was interested so he put in a referral using my CV and a short description.

*Kelsey:* I spent much of Fall 2020 finding, applying for, and interviewing for internships. So instead of going on the job market the Fall of my final year in the PhD, I basically did it in my 4th year. Most of the internships I applied for came for my searches and email notifications from Indeed for alerts like "phd data science" and "phd internship" - phd internships are becoming more and more common. However, in the case of  IBM I had actually applied for a different internship and a recruiter reached out over Linkedin  and recommended I apply for the Sr. Data Scientist internship in the Chief Analytics Office. I definitely recommend keeping your linkedin notifications turned on, but also selecting the 'open to work' option on your profile so that recruiters can find you in their headhunting searches!

__What did you wish you knew before starting your internship?__

*Helge:* My advisor told me that SQL will play an extended role, but I had no idea how much. Also, it was difficult to learn about my schedule ahead of time since it's all flexible and depends on the advisor. That said, work/life balance was great so the ultimate schedule was a positive surprise.

*Kelsey:* This is surprisingly a tough one. I went in with a really open mind, knowing that industry might be a good fit or that I might hate it. Coming from Sociology, I feel like we give industry quite a bad rap and prioritize gov and think tank work as being more 'ethical'. However, I had no idea what impactful work I could be doing in industry! I built the prototype/proof of concept for a model that will positively affect 100k+ employees. I’m sure that will be much more impactfulthan my previous publications!

__Upon reflection, how do you think now about the perceived “bad rap”? Has your opinion changed, or not – and why?__

*Kelsey:* I think the 'bad rap' is dying out in sociology, so many people are going into non-academic careers which is opening a wider variety of perspectives for students and faculty. It comes from sociological theory too, like Marx, but I've learned that we also need and live in the society we're critiquing and that academia is just as culpable. Just my 2 cents!

__Did you have a referral? How does it work?__

*Kelsey:* I chatted with some people I knew at different companies before applying, but I don't think I even had a formal referral. The caveat to this is I had a few recruiters reach out over linkedin and every one of those positions that I had applied for I got interviewed for. I guess you can say that a recruiter counts as a referral! I’ll ‘+1’ myself from an earlier note to urge you to make yourself "open to work" on linkedin, it really helped me!

*Helge:* Yes, I did. I worked with my advisor at ICPSR last year. A spot opened on his team, and he asked me if I was interested so he put in a referral using my CV and a short description.

__Did you have to market and/or translate your academic skills/projects when applying for these internships and if yes, how did you approach that? What did you find worked well in doing that?__

*Kelsey:* This is a question I often get because you read about this so much in books like "the professor is in". On reflection, yes, I needed to speak the language of data science on my resume, application materials, and in interviews. In terms of homophily, this makes sense - if you're speaking sociology to a person with a math background, they're not going to understand where you're coming from and are going to think you're a bad fit for the position. The key is to convince them that you, in my case a sociologist/computational social scientist, can do the work required and what extra you can bring to the table. I had some experience at my university working with the data science institute and interdisciplinary work, so that definitely helped. But even showing your motivation to learn business concepts (etc) on an extra coursera course can really benefit you because it shows you're self-motivated and willing to learn. Those are both extremely important things you can signal in your applications! More concretely, I focused on previous Research Assistant (RA) experience and volunteer experience in my application materials. I know not everyone has those, but I feel like many people try to sell their dissertation work as 'project management' and recruiters get tired of seeing that.

*Helge:* For me it was a matter of emphasis. During the three interviews (coding-screen, methods/experiments, coding), each interviewer asked me for a short description of myself and my work. I emphasized that I earned a MS in Applied Statistics and that I teach Python workshops at Indiana University. I assume it helped because the interviews gauge the extent to which applicants can work independently when hired. That said, I never asked / got feedback on specific parts of the interviews.

__What's the goofiest big tech habit/jargon/quirk that you picked up during your internship?__

*Kelsey:* So many tech phrases... the best I can think of right now is "let's double click on that", that always makes me laugh. My intern-"buddy" (a FT mentor) told me to figure out what they all mean, but try and avoid using them as long as you possibly can! Her point was two-fold : You might use them wrong, but also why try so hard to fit in when you're going to stand out?

*Helge:* “I want to circle back to an earlier point…” became a staple. Meetings move fast and it’s not always easy to get your point in at the right time (especially on Zoom.)

*Kelsey:* And ‘+1’ing everything!

__What did you think was going to be the most challenging part of the internship? What was actually the most challenging aspect of your internship? How did you meet those challenges?__

*Helge:* I assumed that I needed to sharpen my deep learning/ML skills. None of that mattered. Ultimately, I had to learn SQL and how to communicate effectively. The biggest challenge was to learn when to stop. With the amount of data available, it is tempting to test anything and everything, but I had to learn that some questions are not worth the effort and how to distinguish between solid and unreliable assumptions. During the internship I sharpened my intuition on this, but I probably still had a long way to go. It’s something that comes with time/experience.

__Can you explain how you view the similarities/differences of your research area /methods in internship v academia?__

*Kelsey:* For some who go to internships in more research-focused work, or even UX, there can be a lot of overlap! In my case, however, the closest overlap is the team I'm working on. I work under people analytics, which focuses on improving the workplace experience for IBM employees. Inherently that means that I'm working on people problems so my background helps, but I'm not using my topical expertise everyday. That being said, I quickly picked up concepts like design thinking and putting the user-first, something that non-social scientists struggle more with. 

*Helge:* I would describe the differences as “Broad rather than deep.” In Sociology we are taught how to interpret every facet of a linear regression / logistic regression model, but we spend relatively little time on exploratory data analysis / descriptives. We also seldom have the chance to test our idea with multiple datasets, but in industry there's new data every day!

Can you say more about the design thinking stuff? How would you explain it to a social scientist unfamiliar with it? How did it matter to you or your work, and what did you think of it?____

*Kelsey:* Great question! IBM hosts a free course on Enterprise Design Thinking (https://www.ibm.com/design/thinking/page/courses/Practitioner ) that I worked through that really focused on how we can build products around whoever the user is, whether an IT technician, social media user, etc. It's aimed at everyone from research to project management because it tries to focus human-centered outcomes in anything you're doing. A very sociological perspective coming from the business world! It's about brainstorming disparate impacts on different ways that people use your tools, which I think social scientists, especially qualitative social scientists, excel at. I'm still working on how to implement this, but it has come into handy when thinking through how to build a long term plan for a project and what to prioritize first. To clarify, I explicitly reference this course because a) it's free and b) it actually helped me transition into a business frame of reference and re-interpret my skills from that perspective. I gain no benefit if you take it but you do!

__How did you tell your story (i.e. sell your skills and expertise) to land your respective roles? How will you tell that story differently now that its over?__

*Helge:* I emphasized technical skills/knowledge but wasn’t prepared to talk about my dissertation. I later realized that most of my colleagues had a PhD (Psychology, Political Science, Economics, etc.). Going forward I will work on an elevator pitch for my dissertation that resonates across disciplines.

__How about post-internship experience? Do you still keep in touch with your colleagues, and keep working with them?__

*Helge:* At 5pm on the last day my laptop was shut off and suddenly all my usual communication channels were down – which felt weird after 12 weeks of intense work together. But I networked during the last week and added colleagues on FB/LinkedIn. I’m still in contact with some of them about a potential collaboration to do with my dissertation data.

__How are you navigating post-internship life?  Are you considering returning as a full time employee or doing future internships? What's helping inform your decision-making on this?__

*Helge:* I definitely want to finish my PhD but I am thinking about industry jobs after graduating. For the moment, I hope I’ll be invited back for a return internship next summer. FTE after graduating would be great, but as an international student I know that there are a lot of burdens related to my visa situation. During my internship I actively connected with the European offices just to put myself on the map for potential future internships/FTE. More generally, I try to talk to industry insiders to get a sense for how unique my experience was. Someone said, "Yeah you want to make sure you didn't kiss a unicorn" and I think that's exactly it.

*Kelsey:* For me, considering a return FTE offer! The biggest impact here is making sure I complete my dissertation in time, but luckily the FTE start date is flexible. Another lucky thing is that I get to work on my dissertation without worrying about the academic job market. Other considerations that I'm working through with a keen eye to #industry_mentorship is negotiating pay and other considerations.

__Did they send you any cool swag?__

*Helge:*  yes, I got a comfy sweater, two mugs, and a towel. Oh and wireless headphones (but my girlfriend took those.)

*Kelsey:* They did! so much swag! I received two mugs, a tshirt, a backpack, a qi phone charger, and some other desk accessories. My favorite is an acrylic desk name plate that says ‘2021 Intern’ with the IBM logo.

__Did you have to juggle both grad school work and intern work at the same time? If not, how did you arrange that? If so, how did you manage that?__

*Helge:* To stay on track with my dissertation I picked a summer internship. Of course, schoolwork didn’t fully stop during the summer, but my advisors understood if things came in late etc. I’m not sure I could work both jobs at the same time and perform well in either.

*Kelsey:* I would say I managed it poorly. Like Helge said, dissertation work doesn’t fully stop. I was supposed to finish a paper presentation for ASA and ended up having to pull the paper because after 5pm every day I just couldn't get myself to do overtime for dissertation work. That's definitely hard and I recommend trying to clear your schedule for the summer.

__How did you think about “research impact” prior to your internship? Has that changed at all since your internship, and if so, how?__

*Helge:* My personal take is that impact is not something one can plan. I enjoyed that during introductions, we were advised not to focus on impact at FB but just to show up and do our jobs. That’s how I want to think about my dissertation: If nobody reads it but I am convinced of its merit, that’s good enough for me.

__What were the characteristics of more senior people at FB / IBM that made them amazing or horrible to work with? Are there any things that you are considering emulating yourself?__

*Kelsey:* compassion, trust, leading the way and explaining the answers to silly questions rather than having a ‘heavy hand’, good strong communication & organization on team short term and long term goals! I appreciated when I was given the room to pave the path to some long term goals and wasn’t micromanaged.

*Lucy (AMA host):* As we near the end of the hour of this awesome AMA, it also means we’re close to the end. Thank you so much Helge and Kelsey for taking the time to answer people’s wonderful questions!!! I certainly learned a lot!

*Helge:* Thanks everyone for the great questions! Thanks for organizing, Matt, and Kelsey for doing this together. I had a blast!

*Kelsey:* Thanks to everyone for the great questions!

### Sam Jaroszewski, PhD, and Michael Dickard, PhD (April 22, 2021)

*Sam Jaroszewski is a UX Research Lead at Yahoo. She's a mixed methods sociologist who runs surveys, usability studies, interviews, and more each week. She's a first-gen, Princeton sociology PhD alum, and resident Chicagoan.*

*Michael Dickard is a sociologist and UX Research Manager at Vanguard, where he helps lead a team of 25 UX researchers spanning work across their new mobile app, robo-advisor, and personal financial advisor services. He has been focused on growing his team, optimizing research practices and processes, helping researchers thrive, and supporting the strategic direction of the platform's digital experience. He has  also led research across the product life cycle, from product discovery and launch through post MVP refinement. Prior to industry work he received a M.A. in Sociology and Ph.D. in Information Science.*

*Sam:* Hi all!! Sam here, a UX research lead at Yahoo working on all things Sports & Fantasy Sports. I'm in sunny (but cold!) Chicago listening to ["acoustic covers 2021"](https://open.spotify.com/playlist/4uf0gphPVNtk4Uu0bxtdGA) on Spotify and excited to jump into your awesome AMA questions, and hear from my old friend (and fellow former Yahoo UX intern), Michael!

*Michael:* Booting up my 12 year old computer from my PhD days so I can type on something other than a phone. :) Excited to hop in and answer all these great questions with my friend, Sam!!

__What’s the most common piece of advice you find yourself giving to academics who are interesting in moving to industry?__

*Sam:* Prepare!! Even though you have transferrable skills, it's a different type of skilled work, so spend a lot of time looking at job ads on linkedin for the type of jobs you want, and work backwards from there. How do people in that job talk about the work? How do they prioritize the key skills? How do they frame the types of day to day tasks? See if you can talk to people in those roles to better understand how to set yourself up for success (like any of us here in the industry mentorship channel!). Toggle your linkedin profile to "open to new opportunities." If you're a PhD or masters student, my #1 piece of advice is INTERNSHIPS! Happy to talk more about that.

*Michael:* First, network and meet people outside of academia! There are so many amazing, talented people who have different skills than you in industry - many are interested in learning more about research (a thing you know tons about!), and they can often help you learn more about another space (e.g. design, industry, engineering, etc.). Even within academia, try to attend conferences where recruiters go, if you can (e.g. CHI, CSCW, etc.). Second, practice your talks and think about how you can reframe your work to tell a story for different audiences - focusing more on methods for the researchers, and more on the actionable outcomes and how you worked with others for people such as designers and product managers.

*Sam:* YES! and if I could add one more thing: learn to tell your story BRIEFLY. I find that when folks ask me to talk about transitioning to industry work, I now heavily caveat my question "please tell me about your work" to "BRIEFLY, please spend a moment or two giving me an OVERVIEW of what you work on" because in my experience, that question often leads to like, a 15 min chapter by chapter outline of a dissertation, which unfortunately is rarely relevant to the goals of our chat. Give a SHORT summary and then say "is there something in there you'd like to hear more about?"

__Is there anything you miss or feel like you closed the door on by switching to industry?__

*Sam:* I miss teaching! I scratch this itch by mentoring our interns, co-leading our internal (to this group) mentoring program, volunteer with Built By Girls and convince my prof friends to let me guest lecture in their classes. :slightly_smiling_face:

*Michael:* Hmmmm. Honestly, not really (re: close door on). I still guest lecture, meet up virtually with students, and read academic papers (I still submit and review papers too, but much less frequently). I do miss aspects of academia though, for sure. I love having time to think deeply about a topic. This might be a bit the nature of my work now as a manager (who also does some research still), but I don't have as much time as I'd like to keep up on the literature anymore. I'm a twitter lurker and LOVE all you folks who are active in sharing your research and insights there - it helps me find balance with regards to maintaining a sense of connection to academia still.

__What do you wish academic departments could do better to help make the transition to industry easier for students?__

*Sam:* Along with others here, I've been thinking a lot about this. I wish faculty encouarged students to CONSIDER and train for a broader set of outcomes (i.e., not just traditional academic jobs), integrated career resources into the curriculum, encouraged and supported students to intern (!!!) over summers, and invited industry professionals to give talks or panels - much like academics do - to help expose their students to a breadth of career paths.

*Michael:* Good question! I have worked with a couple of Universities now who are doing an amazing job on preparing students for industry work, and the thing that stands out to me are how well prepared these students are with their research portfolios and interview prep. So I'd recommend training or supporting students in this area. I know a lot of sociology departments don't necessarily know how best to support students in these areas for industry jobs. I'd recommend they explore ways to do this though: maybe reaching out to other departments at the university or building relationships with industry folks and inviting them to talk? Networking with some industry folks so they can point students to resources and events (e.g. places like this!). Also, just raising awareness of the fact that academia isn't the only route for their students.

__Curious how you balanced being a sort of "expert" and (presumably) having a lot to learn in industry. What approach did you bring to the table to tackle problems with a deep social science knowledge, while also acknowledging that you had a lot to learn in/from industry?__

*Sam:* I don't have a great perspective on this one because my dissertation was about Fantasy Sports, my field site was Yahoo (I studied the engineering teams who build it AND the users!), and my job is now at Yahoo, so I have an incredibly niche level of expertise about my users, my engineering team, etc. I think folks recognize that PhDs are experts in methods (I emphasize that I'm a social scientist as much as a sociologist) and matching research questions to the right method. Leaning into that helps. I also like Mindy Kaling's "Why Not Me?" essay on how we earn/gain confidence through learning to be good at our jobs (I'm not explaining it nearly as well as as she does of course).

*Michael:* One thing that took me a while to get used to was what it means to be an "expert" and my perception of that changed when joining industry. Everyone in academia was my peer or senior to me when it came to conducting research. In Industry I was suddenly the senior expert on research, but it took me a while to realize that others often thought they were experts in research too, even if they weren’t. I had to learn to trust myself more and figure out how to translate their needs and goals into research questions I could answer and would address the core needs they really have.

Also, with regards to the second half of your question, I generally like taking an "apprentice" role, especially in the first few months anywhere I work. I like to think about it similar to starting off an ethnographic project, where I'm an outsider to start. Later on, I just always try to collaborate and make people feel involved and part of the process when it comes to research, and then try to learn from them when it comes to their areas of expertise.

__I hear a lot of sociologists in particular worry about how transferring into industry will make them a "sell-out". How do you feel about this sentiment from the academic fields and if you experienced that particular dissonance, how did you deal with it?__

*Michael:* I think it's bullshit tbh lol. I don't think I've been around this sentiment as much, so I'd have to dig a little deeper to understand where they might be coming from. If someone brought this up, I'd probably try to understand more; I'd also think about the positive impacts I have on people's lives through my work. One of the things I remember being a big topic of interest during my Master's program was trying to engage in public sociology, trying to have an impact beyond the publication system was important to us there. I feel like I live that every day now.

*Sam:* Perhaps uncharitably, but I would say those people probably shouldn't transition to industry, in the same way that someone who feels like academics don't have impact should not be an academic. This isn't a concern I share or hear with any regularity. I think of industry work -- and the inclusion of sociologists at the table where important decisions about technology are being made --- to be high impact, high meaning, and high skilled work. Curious to hear more about how others feel.


__Another thing that I've heard some people describe is this sense of 'starting from the bottom'  again when they join industry after a PhD, which is scary once you feel you've already had a lot of credentials and you dont want to be junior most/ small fish again. Is this something you had any concerns about?__

*Sam:* This is REALLY hard and is why I think internships are so important. If you're senior by virtue of having a PhD, but you have never done a UX job before, you can't come in as a senior position because you can't actually start doing the job on day 1. You'd be entry level by virtue of not having experience with the job at all. That makes leveling really hard. Credentials are obviously meaningful and signal that you can learn, deeply, but they don't automatically qualify for a skilled job, so I think being really mindful about that and approaching the job with a genuine curiosity, being cognizant of what you DO and what you DON'T (yet) know goes a long way, and taking steps to get hands on experience -- whether it's an internship, a short contract stint, a really good course on how to do the job, etc, goes a long way. I personally have never minded being a small fish, I love feeling like I have a lot to learn from others in every room I am in.

*Michael:* I personally find this thrilling, learning a new space and company. It's like a research project unto itself. I try to think of it as a learning opportunity that will help me grow and further develop in my career and as a researcher. I definitely get the sentiment though and felt that way at the beginning of my career. Hopefully any place you go to work will have awesome people that don't view you as a small fish, but as an amazing person that can contribute to the team! They hired you because you're awesome and see tons of potential in you, at least that's my pov as a manager. =)

__I’ve been thinking about this a lot, especially as a tech worker with funds available for trainings and skilling up. What skills do you think about building as you look forward in your career?__

*Sam:* Conferences are the main ways I've used this kind of discretionary funds, and I might re-buy Stata, just because I'm so much faster in it than anything else. I keep re-starting R classes on linkedin learning and i just have no time. TBH, I feel like I don't have the time for this though I wish I did. I'd prob invest in R and maybe data visualization? Also trying to ramp up my experimenting internally, not sure how i'd beef that up though.

*Michael:* So much to say about this! We (me and the head of UXR where I work)  developed career path documents not too long ago. We have different skills (e.g. Research, business knowledge, etc.), standards of behavior (drive outcomes, be candid, etc.), and areas of impact/outcome. We work closely with people to explore where you stand on each of these and identify opportunities for you to grow and develop. Where this is all going is that I’d want to sit down with you to look at where you are already thriving and doing awesome work, and where you’d want to develop your skills further, and come up with an individualized plan with you.

Generally speaking, if you are coming out of a phd or master’s program, I’d say that Understanding business strategy, prioritizing projects, and design thinking are three areas that I often have new researchers focus their efforts. These things will help you produce more impactful results with your work imo.

__What should a sociology PhD student put in their resumes (or CVs?) to get a job in UX Research?__

*Sam:* Look at job listings you want, and phrase the skills you HAVE to mirror the way they're posted as desired skills. take the list of required skills you don't YET have as a call for where you can invest in your professional development to fill the gap, or be prepared ot say how you hope to address that skill gap. I focus on qualitative and quantitative methods on my resume, as separate skills listing. qual = interview writing, conducting and analysis, ethnography, contextual inquiry, usability studies, diary studies, etc. quant = survey construction, fielding and analysis. statistical software proficiencies, etc.

*Michael:* I look for a few things when skimming resumes: your role on projects (were you leading a project end to end? conducting interviews? analyzing data? etc.); methods used; level of expertise/experience with certain methods; collaboration; topics/domains where you have done research. The last item being more or less important depending on the role I'm trying to fill.

*Sam:* This is important! make this info easy to find on resumes. in our hiring meetings, we often go through resumes fast and I find myself saying, "its hard for me to tell X from their resume..."

__What is the actual process of interviewing and applying for industry jobs?__

*Sam:* For the most part, in my experience, it's like this: recruiter contact; recruiter phone screen; initial screen with a researcher (or maybe hiring manager); technical interview with a researcher; panel interview with a research exercise. And the recruiter is someone who can help you along with the process, and the researchers and hiring managers are the people you're trying to impress. If you have anything that feels liek a "dumb question" -- what should I do? what are the expectations around this? when will i hear back? the recruiter is your ally! As an interviewer, i find it stressful when candidates ask me questions that should go to the recruiter (including salary, start date, etc).

__What does a UX researcher do on a daily basis? That would help me understand how, for example, quantitative UX research is different from research analyst/data science positions.__

*Sam:*  I would say every day my job is to advocate for our users, with the "north star" of that advocacy being to improve our key metrics of success (these are usually business things like engagement, and daily average users). The way I do that on any given day might be usability studies, user events, surveys, diary studies, or something else. Once a study is done, its important to follow through with key stakeholders (PMs, Design & Engineering) to get the top recommendations implemented, and then follow up to measure the impact.

*Michael:* This might vary a bit by company, type of company (e.g. in-house vs. agency), stage of your career, and whether you are a contractor or full time employee. The core of it is collaborating with a team of functional experts and leading research to support this team in solving problems or helping people (e.g. clients/customers/stakeholders) meet their goals or complete tasks. The biggest differences compared to academia are that your work in industry tends to be agile, quick, iterative, and collaborative by default.

__How do you as sociologists working in industry relate to professional associations like the ASA generally, and their Codes of Ethics in particular?__

*Sam:* This is not something I think about often, TBH. My ASA membership lapsed this year, and without an IRL ASA I'm not sure what I'll do. I did present dissertation chapters in the last IRL ASA but haven't engaged since.

*Michael:* Had to look them up, it's been a while since I looked at them. I'll speak to my current role, but it has varied depending on where I work. I'm lucky that the company I work at now is structured very differently from most other companies (we are owned by our clients/customers), and ethics are integral to everything we do. Integrity and social responsibility are core to who we are as employees here. Of course, there is always room for continued improvement though.

__How do you navigate the question of NDAs and publications?__

*Sam:* This is all handled at the company level! We have strict NDA policies that our ops team handles. We have a standard one we include in  all surveys, but every user who comes in for qual research also signs our company NDA. I talk to our legal team weekly, and would start there to get legal review for a publication. Our team has fewer academics than it used to, but we're expected to publish so there are processes in place. Basically, we're not allowed to share anything before its been released or proprietary data. typically the kinds of things I'd publish -- like my dissertation, which was about how users think about and talk about their fantasy sports communities -- were totally in the clear. they did scratch some of the demographic data I included about the teams at work I studied because the small teams were fairly identifiable.

__Since both of you are in UX, how do you distinguish quantitative UX research from other analytics roles? How do the goals and/or perspectives change?__

*Michael:* Good question. I think this will vary by company, within companies, and within teams within companies, to some degree. Where I work, we are embedded on teams, so researchers and analysts/data scientists work closely every day. Setting expectations early, prioritizing work together, and thinking about each of our roles as supporting how our lab learns (labs = teams of people focused on a particular product, journey, or capability) is key. Beyond that, I would recommend reading this article because I don't think I could put it much better than [this](https://medium.com/facebook-research/how-quantitative-ux-research-differs-from-data-analytics-1bbf0903768b).

__Both of you have been vital mentors to others - has your approach changed over time? Have your mentees’ inquires changed? How do you tactfully say no to the many (bc of bandwidth)?__ 

*Sam:* Good question, and thanks for the kind compliment. This group has helped me say no more, because it has given me a sense that I am doing enough and it also helped me scale up my ability to reach folks. I heard a panel once where an exec woman said she has a set number of mentee "slots" and so that rule helps her say "no" when more people ask, and she can keep a short waitlist. You can set yourself a limit for hours per month to mentor or answer linkedin requests. Having this group to point people to AND having the doc "so you want to transition to industry" (pinned in the industrymentorship channel- go check it out!) helps me start intros with "Go check out this resource I helped put together that answers FAQ,a nd if you have follow up questions, let me know and i'll do my best to answer them." I also now say I will answer questions via email but prefer no more calls (I'm so tired of calls. :sob:)

*Michael:* Yes! I have a lot more visibility into hiring practices now than when I started mentoring, so I can help people think more about how to be successful when applying places than I could in the past. I also value prioritization and thinking about how to be impactful in different ways than when I started out. Inquiries have been similar over time though. Pre-covid, I tried to attend local events and point people to them, letting them know I'd be happy to chat there when it became too much to do one-on-ones. Now, it's a bit more challenging and even the few people I've committed to mentoring I have trouble keeping up with tbh. I hate saying no, too.

__Did you have mentors that helped? In what capacity? How did you meet? If not, do you wish you had one?__

*Sam:* I've been really lucky to feel like mentors keep appearing in my life, I have some fairy godparents at work and their support means the world to me. Interestingly, they are not in research roles: one is our product vp for sports and the other is the director of design. we have regular 1:1s that are entirely for my professional development, and I have really grown from these. I have also asked one person to be a mentor- our former product lead for fantasy who left and I had this epiphany that I had taken for granted that I had many more years to learn from him. On his last day, I asked if he'd consider meeting with me once a quarter on an ongoing basis for a mentor/mentee relationship, and it brings me joy and lots of new perspective.

*Michael:* Yes! I had friends who did internships, they helped me so much in getting my first internship. Once I started my internship, I was surrounded by amazingly talented folks that I could learn from. After my internship, I did a contractor-converted-to-full-time role and my co-workers served as mentors while there too. Along the way I made friends and co-workers and local people who all gave me advice and whom I learned from along the way - so no one was really a "mentor" prior to entering, other than some friends who were interns giving advice.

__Where did you learn UX skills or learned about the field before getting an internship, or a permanent job? I feel like not having taken any UX research class, I’m learning about UX research lingo such as generative research, evaluative research on my own, via reading Medium articles, watching Youtube Videos. Do you think this self-learning process is necessary for a PhD student who is looking to transition to industry?__

*Sam:* I learned most of the specific skills on the job, during an internship. I feel strongly that internships are the most straightforward path to making this transition, and recommend them as often as I can. I think that the self learning process is 100% necessary, and it is very evident in an interview process when a candidate has not attempted to learn the skills and lingo required for the job they've applied for.

*Michael:* I first learned of UX from talks in my sociology department - my advisor brought in a data scientist from Facebook to speak with us. Then, over time I met more people at conferences who had a UX research role in tech, read a lot of articles online, etc. But it took me a while to start thinking more about design research. Reading papers in HCI and CSCW also helped me think about how to frame questions that relate to products, design, or technology use more specifically. This helped me a lot too when thinking about how my research can be actionable within a company. And yes, I would say self-learning is something we as PhD students are great at! It's something you truly are an expert in and helps so much with transitioning to industry, imo.

*Sam:* OK!! I think that's all the questions, I didn't have an answer to the "whats the difference between quant UX and data science" so I skipped but looking forward to seeing other discussions that are in the replies. I'll go back and check any convos in the replies, but THANK YOU ALL for the questions. This was a delightful way for me to wrap up my day. :heart: rooting for all yall!

*Michael:* Alright all - time for me to sign off too! Can't wait to go back through and read the conversations a bit more.

### Alex Hanna, PhD (January 27, 2021)

*Alex Hanna is a sociologist and senior research scientist on the Ethical AI Team at Google Research. Before that, she was an Assistant Professor at the Institute of Communication, Culture, Information and Technology at the University of Toronto. Her research centers on origins of the training data which form the informational infrastructure of AI and algorithmic fairness frameworks, and the way these datasets exacerbate racial, gender, and class inequality.*

All right, I’ve had my second cup of coffee, I’ve put on [Chromatica](https://en.wikipedia.org/wiki/Chromatica), and I’m ready to do this. Thanks so much to *Daisy Lu (she/her)*, *Matt Rafalow (he/his)* and *Angèle (she/her)* for inviting me to do this. I’m very honored to be the inaugural AMA question-answerer. I’ll try to do my best!. As for answering questions, I’ll reply to them in the thread but also send them to the channel. Here we go…

__What first got you into the study of digital technology from a social science perspective? What inspired you?__

I’ve been a nerd for years. Since I was 4. I’ve known that I wanted to do something with computers since those early days. Initially, I thought that looked much more like computer science and so I did things like follow computer-ish trends, looked at Best Buy catalogs :satisfied:. I should have learned some programming in high school. So I majored in CS when I got to university. But soon after, I realized that I had a lot more interest in people and the social aspects of technology, rather than the technology itself. 

After undergrad I went to a coffee shop in Chicago (where I was working as a software developer for a web company) and picked up a NYT magazine article on Egyptians using Facebook to organize protest. This was in 2008, before the revolution. So when I got to Wisconsin for sociology, I collected a bunch of data on the Facebook group they used and used machine learning to understand patterns of mobilization. That turned out to be my master’s thesis and my article in [Mobilization](https://alex-hanna.com/static/pdf/Hanna.Moby2013.pdf). It was a real niche area at the time, to study digital stuff. Not a lot of people were doing it in sociology back then. But that’s what put me on that path.

__If you could go back in time and talk to Alex the PhD student, what would you tell her to do differently and what would you tell her do to more of/continue doing?__

Oh boy, this one, LOL. Hindsight being 20/20, I think I’d have spent more time studying sociology of race and ethnicity — which I think a lot of the fairness/AI ethics conversation has a serious deficit -- and trying to do some projects which would have allowed me to do ethnography. My initial dissertation project was this overly ambitious thing where I wanted to do surveys, ethnography, and quanty computational social science of Egyptian revolution trace data. I applied for a Fulbright and, while the US government was fine with it, the repressive Egyptian government surely was not. So I ended up doing a really quanty, methodological dissertation. It was a risk, and my advisor said it was a risk, but I still did it! I probably would have done a dissertation which was much more catholic (in the 2nd definition sense) in terms of method.

I think I’d actually tell student-Alex to keep studying the computational, though. And to keep on doing activism and organizing.

__What was the vibe (for lack of a better term) of  digital scholarship at the time, around 2008?__

There was a lot of it! Just not in sociology. I found weird pockets of community, at, like, ICWSM. I remember complaining to danah boyd at ICWSM in 2011 that I wanted to bring sociologists and she said she tried to drag computer scientists to the big anthro conference. But at the time, AoIR was around, as well as the communications conferences.

Folks knew that this was important, but they were far from the center of the discipline.

__What do you find most challenging about the work you do? Most rewarding?__

Most challenging: dealing with engineers and people who want technical solutions to social problems.

Most rewarding: getting people to realize that social problems are hard for a reason? Haha. But honestly, I think some of the more rewarding parts of the work is the mentorship I get to do and seeing students I work with succeed.

__If you could teach everyone in the AI and technology space one takeaway from the social sciences what would it be? And if you could have all social scientists learn one takeaway from  the AI and technology space, what would it be? (said another way, what is the main thing these two areas can learn from each other?)__

Ohhh I like this one. People in the AI and technology space, I think, need to learn that the technical cannot be divorced from the contextual. But I think that needs more meat on its bones. What I think that means is that the technical can’t be divorced from the institutional. Technical artifacts interact with organizations at multiple levels.
As for social scientists… hrm. This one is harder for me to answer because I’m not sure there’s anything necessarily “foundational” in AI as a research program that social scientists can learn. That sounds shadier than I wanted it to, but I don’t know if there’s another way to put that. Lemme mull on it for a bit.

__As someone with experience both in academia and industry, what would you recommend to PhD students applying to both types of job? I love your research on dataset creation/infrastructures, AI genealogy, and AI and society broadly speaking. What do you think are the biggest gaps in AI research that sociologists can address?__

Thanks for your :heart: :grinning:

First question: people in industry look for different things in a particular candidate. Hiring managers are typically looking for a skill set which can translate easily to industry. *Matt Rafalow (he/his)*, *Iga Kozlowska (she/her, ee-ga)* and others have written a lot on this, so I’ll defer to their excellent pieces on this. Academic departments are usually looking for publications and ability to present and teach. The two aren’t necessarily in opposition, but it depends a lot on where you publish to be legible to both. If you’re looking to try to apply to both, say, UXR positions and iSchool faculty jobs, then you probably want to target publications like CSCW and HCI. But those publications wouldn’t be legible to, say, a sociology department who wants you to publish in journals.

Biggest gaps in AI research: geez, talking to people affected by AI. Understanding institutions where AI operates. *Angèle (she/her)*, Sarah Brayne, *Elizabeth Watkins*, and Madeleine Clare Elish provide some great models of how and where to do this work ethnographically.

__As someone working on such important, mission-driven work, what do you do to re-charge so you have the energy to keep pushing for what matters?__

Oh my gosh. I’m still trying to figure it out. Take breaks. Take time off. Take naps. I nap every day. I used to be fervently against naps. Why. I miss roller derby, but exercise is good. Do something outside of your job where you aren’t thinking of your job. Find solace in community and family.

__I've long wondered but it always felt too weird to ask, but here it goes: what is a "bad Hessian"?__

:laughing: For those of you who aren’t aware, we used to run this computational social science blog called [Bad Hessian](https://badhessian.org/) back in the day. Adam Slez (faculty at UVA) and I started it when we were at Wisconsin. A [hessian matrix](https://en.wikipedia.org/wiki/Hessian_matrix) is used in calculating coefficients for some types of maximum likelihood models. I know very little about how this actually works. Adam does, because he’s much smarter than me in the quantitative dark arts. Anyhow, the R programming language gives you back a cryptic message like “bad hessian” when you don’t have enough data to calculate a something-something regression. There’s a lot of layers to this joke. I can’t believe I’m typing this all.

__What is your setup for work, hardware and software-wise? Feel free to address or avoid important cultural issues like: Vim or Emacs? Spaces or tabs? R or Python?__

Hard hitting questions here. I use a MacBook Pro. Don’t ask me what kind. I have a big monitor. Don’t ask me what kind. Emacs 4eva, although most of the time it’s Visual Studio Code. Spaces, I guess. Python, if I can help it, but I do a lot of work in R for a [project](https://www.ellenberrey.com/research) I’m doing with Ellen Berrey.

__Was industry something you’ve always been interested in? In other words, was industry something you thought about in graduate school or was the decision easier to make after teaching? (A follow-up would be: what advice do you have for students interested in industry) How have the events of the past year (and summer especially) affected your research with respect to inequality?__

I haven’t always been interested in industry, but I knew it was an option. I thought about it when I was in grad school but I didn’t know how to break in. I also didn’t see a lot of sociologists who were able to get into industry. Nobody in my grad program was telling us how to get industry jobs. For advice, gonna defer to the answer above, which defers to others’ blog posts.
I think you’re referring to BLM protests this summer? Which, if that’s the case, it deepens the need to have racial inequality — and specifically, whiteness and the pervasiveness of anti-Blackness — at the center of sociological and technical analysis, especially in conversations around “fairness” and “justice”.

Oh gosh we only have 17 minutes left. I think I’m going to put [Carly Rae Jepsen](https://en.wikipedia.org/wiki/Emotion_(Carly_Rae_Jepsen_album)) on now.

__Do you have any dream projects? If yes, why those projects specifically? What would you want to do with the data/findings?__

In a way, I think my dream is doing what I’m doing now: Focusing on benchmark datasets, understanding everything I can about those datasets, about their histories and trajectories. I think if I could deepen it more, I would partner with a historian of science to do some archival work. In terms of what to do with results? There’s definitely a lot of room for science policy impact here, especially changing the way these datasets are created and evaluated. Also there’s that book that’s in the offing…

You have traversed many disciplines. You started out in CS, mathematics, and sociology; then focused in sociology as a graduate student; and as an assistant prof and now industry researcher you increasingly publish in HCI. Can you share a bit about this journey, including what shaped this path and what you have learned along the way?

This question is so broad :laughing:. I think my journey has been a weird one, like a careening, silly kind of gradient descent. I’ve learned a lot about what are not acceptable local minima for me: being purely in industry from an engineering perspective (I interned at Microsoft in 2006 and it was awful. Hopefully if I ever apply for MSR they don’t find my exit interview). I was faculty and it was better. But now I landed in a place which feels good and right for now (at least, it did before Jeff Dean messed everything up). I think I learned that your first record doesn’t have to be your best record, that a job is a job, and you gotta find a place that appreciates you for how you work and who you want to talk to.

__In considering what PhD programs to apply to, I've gotten advice from my PI that it's better to establish your expertise in one discipline before you start being interdisciplinary (particularly because people in academia and industry want to know what you specialize in). Do you have advice for anyone who feels lost finding their disciplinary home?__

I feel this intimately. I think you do need a home. I would suggest talking to people across disciplines, taking classes in different disciplines, really trying to get the feel of that discipline. Although this isn’t even a satisfying answer to myself… So maybe some better advice is… find the people who are doing the stuff you want to be doing. Talk to them. Ask them whether their path is one you could/should emulate.

__How do you organize your days? Which series do you watch (it's an AMA right?). And what's the deal with ROLLER DERBY??? We all want to know (and imitate) :heart_eyes_cat:__

Before the mess with my team (and when I was being good to myself and self-blocking Twitter), I was doing a very good job at waking up around 5 or 6 and writing until about 9. Then taking whichever meetings after that, and finishing up around 4 or 5. I’d then workout after that or have roller derby practice.

What do you want to know about roller derby??? It’s a silly ass sport where you put wheely shoes on your feet and hit other people. Find me playing [here](https://www.youtube.com/watch?v=OEahntLSwL4) under my nom du skate Kate Silver.

I just finished Star Trek Voyager and have moved on to Star Trek Enterprise. I am trying to be a completist and watch every Trek (except for The Original Series, which is Bad).

***

![bookshelf with closeup of books](/img/books.jpg)
Photo by [Ugur Akdemir](https://unsplash.com/@ugur)
