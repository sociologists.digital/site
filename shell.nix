{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/20.09.tar.gz") {} }:

with pkgs;

  stdenv.mkDerivation rec {
    name = "env";
    env = buildEnv { name = name; paths = buildInputs; };
    buildInputs = [
      python38Packages.emoji
      python38Packages.mistune
      python38Packages.smartypants
      python38Packages.staticjinja
    ];
  }
